import React from 'react';
import classes from './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';

const buildControls = (props) =>{
  let controls = Object.keys(props.ingredients)
                    .map((label,  index) => {
                         const disabled = props.ingredients[label] <= 0 ;
                         return  <BuildControl
                             clicked={()=>{props.addIngredient(label)}}
                             removed={()=>{props.removeIngredient(label)}}
                             key={label+index} label={label}
                             disabled={disabled}/>
                           });
  return(
    <div className={classes.BuildControls}>
       <p>Total Price {props.totalprice.toFixed(2)} $</p>
       {controls}
       <button
           className={classes.OrderButton}
           disabled={!props.isPurchasable}
           onClick={props.showmodal}
       >
           ORDER NOW
       </button>
    </div>
  )
}

export default buildControls;
