import React from 'react';

import Button from '../../UI/Button/Button';
import Aux from '../../../hoc/aux/Aux';
import classes from './OrderSummary.css';

const orderSummary = (props) => {
    const ingredients = Object.keys(props.ingredients)
    const list = ingredients.map((ingKey, ind) => <li key={ingKey + '__' + ind}> {ingKey} : {props.ingredients[ingKey]}</li>);
    return (
        <Aux >
            <div> The Burger Name</div>
            <p> Ingredients</p>
            <ul className={classes.OrderSummary}>
                {list}
            </ul>
            <p> Total Price {props.totalPrice.toFixed(2)} $</p>
            <div className={classes['button-box']}>
            <Button type='cancel'
                  click={props.cancel}>
                   Cancel
            </Button>
            <Button type='checkout'
                  click={props.purchase}>
                   Checkout
            </Button>
            </div>
        </Aux>
    );
};

export default orderSummary;
