import React from 'react';
import Ingredient from './BurgerIngredient/BurgerIngredient';
import classes from './Burger.css';

const burger = (props) =>  {
  const ingredients = Object.keys(props.ingredients)
         .map((ingKey, index) =>{
             const count = props.ingredients[ingKey];
              return [...Array(count)].map((elt, ind)=>{
                return <Ingredient type={ingKey} key={ingKey+"__"+index+"__"+ind}/>
              });
         });

  return (<div className={classes.Burger}>
            <Ingredient type="bread-top"/>
             {ingredients}
           <Ingredient type="BreadBottom"/>
         </div>)
};

export default burger;
