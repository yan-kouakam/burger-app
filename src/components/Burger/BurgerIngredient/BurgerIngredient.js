import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classes from './BurgerIngredient.css';

class Ingredient extends Component {

  render(){
    let ingredient = null;
    if(this.props.type === 'bread-top'){
      ingredient = <div className={classes.BreadTop}>
                      <div className={classes.Seeds1}></div>
                      <div className={classes.Seeds2}></div>
                  </div>
    }else{
      ingredient = <div className={classes[this.props.type]}></div>
    }

    return ingredient;
  }
}

Ingredient.propTypes = {
  type: PropTypes.string.isRequired
};

export default Ingredient;
