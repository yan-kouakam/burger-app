import React, { Component } from 'react';

import classes from './Modal.css';
import Aux from '../../../hoc/aux/Aux';
import BackDrop from '../backdrop/BackDrop';

class Modal extends Component{

    shouldComponentUpdate(previousProps){
     return previousProps.showModal !== this.props.showModal && previousProps.children !== this.props.children;
    }

    render(){
        const animation = this.props.showModal ? classes.animate : '';
        return (
            <Aux>
                <BackDrop showBackDrop={this.props.showModal} closeModal={this.props.closed}/>
                <div className={[classes.Modal, animation].join(' ')}>
                    {this.props.children}
                </div>
            </Aux>

        );
    }

}

export default Modal;
