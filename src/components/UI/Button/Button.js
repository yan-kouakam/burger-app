import React from 'react'
import css from './Button.css'

const button = (props) => {
    let type = props.type === 'cancel' ? css.Cancel : css.Checkout
  return (
      <button className={[type, css.button].join(' ')}
            onClick={props.click}>
            {props.children}
        </button>
  )
}

export default button
