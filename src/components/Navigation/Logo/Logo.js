import React from 'react';
import classes from './Logo.css';
import BurgerLogo from '../../../assets/img/logo.png';

const logo = () => {
    return (
        <div className={classes.Logo}>
            <img src={BurgerLogo} alt="LOGO"/>
        </div>
    );
};

export default logo;
