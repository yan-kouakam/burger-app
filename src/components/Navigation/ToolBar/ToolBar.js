import React from 'react';
import classes from './ToolBar.css';
import Logo from '../Logo/Logo';
import Menu from '../Menu/Menu';
import NavigationItems from '../NavigationItems/NavigationItems';

const toolbar = props => {
    return (
        <header className={classes.Header}>
         <Menu clicked={props.showSideDrawer}/>
         <Logo/>
            <nav className={classes.Nav}>
               <NavigationItems />
            </nav>
        </header>
    );
};


export default toolbar;
