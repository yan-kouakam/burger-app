import React from 'react';
import Logo from '../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';

import css from './SideDrawer.css'
import Backdrop from "../../UI/backdrop/BackDrop";
import Aux from "../../../hoc/aux/Aux";
const sideDrawer = (props) => {
    let style = [css.SideDrawer, css.Close];
    if(props.showSideDrawer){
        style = [css.SideDrawer, css.Open];
    }
    return (
        <Aux>
        <Backdrop showBackDrop={props.showSideDrawer} closeModal={props.closeSideDrawer}/>
        <div className={style.join(" ")}>
            <div style={{height: "40px"}}>
                <Logo/>
            </div>
            <nav>
                <NavigationItems />
            </nav>
        </div>
        </Aux>
    );
};

export default sideDrawer;
