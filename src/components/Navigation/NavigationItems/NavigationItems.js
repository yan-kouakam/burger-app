import React from 'react';
import NavigationItem from './NavigationItem/NavigationItem';
import css from './NavigationItems.css'

const NavigationItems = () => {

    return (
        <ul className={css.NavigationItems}>
            <NavigationItem linkPath="/" > Burger Builder</NavigationItem>
            <NavigationItem linkPath="/order"> Order</NavigationItem>
        </ul>
    );
};

export default NavigationItems;
