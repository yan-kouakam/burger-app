import React from 'react';
import {Link} from 'react-router-dom'

import css from './NavigationItem.css'

const NavigationItem = (props) => {
    return (
        <li className={css.NavigationItem}>
            <Link to={{pathname: props.linkPath}} > {props.children}</Link>
        </li>
    );
};


export default NavigationItem;
