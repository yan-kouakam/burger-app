import React, {Component} from 'react';
import classes from './Layout.css';
import Aux from '../../hoc/aux/Aux';
import ToolBar from '../Navigation/ToolBar/ToolBar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

class Layout extends Component {
    state = {
        showSideDrawer: false,
    }

    closeSideDrawerHandler = () => {
        this.setState({showSideDrawer: false})
    }

    showSideDrawerHandler =()=>{
        this.setState({showSideDrawer: true})
    }

    render() {
        return (
            <Aux>
                <ToolBar showSideDrawer={this.showSideDrawerHandler}/>
               <SideDrawer
                    showSideDrawer={this.state.showSideDrawer}
                    closeSideDrawer={this.closeSideDrawerHandler}
                />
                <main className={classes.content}>
                    {this.props.children}
                </main>
            </Aux>
        );
    }

}

export default Layout;
