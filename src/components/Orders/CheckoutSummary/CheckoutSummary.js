import React from 'react'
import Burger from '../../Burger/Burger'
import Button from '../../UI/Button/Button';
import css from './CheckoutSummary.css';

const CheckoutSummary = (props)=> {
  return (
    <div className={css.CheckoutSummary}>
      <h3> We hope it taste well</h3>
      <div className={css.Burger}>
          <Burger ingredients={props.ingredients}/>
      </div>
      <div className={css.Button}>
      <Button 
        type='cancel'
        click={props.cancel}
      >
                   Cancel
     </Button>
    <Button 
       type='checkout'
       click={props.purchase}
    >
                   Checkout
    </Button>
      </div>
    </div>
  )
};

export default CheckoutSummary;
