import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Layout from './components/layout/Layout';
import BurgerBuilder from './containers/burger-builder/BurgerBuilder';
import Checkout from './containers/checkout/Checkout';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <Layout>
          <Switch>
          <Route path='/'  exact component={BurgerBuilder}/> 
          <Route path='/checkout' component={Checkout}/> 
          </Switch>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default App;
