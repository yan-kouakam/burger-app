import React, {Component} from 'react';

import Burger from '../../components/Burger/Burger';
import Aux from '../../hoc/aux/Aux';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/spinner/Spinner';

import axios from '../../axios-order';

const INGREDIENT_PRICE = {
    Salad: 0.6,
    Meat: 1.6,
    Bacon: 0.9,
    Cheese: 0.7
};

class BurgerBuilder extends Component {
    state = {
        ingredients: null,
        totalprice: 2,
        purchasable: false,
        showModal: false,
        loading: false,
    }

    updatePurchasable = (ingredients) => {
        const ings = {...ingredients};
        const num = Object.keys(ings)
            .map(key => ings[key])
            .reduce((acc, val) => {
                return val + acc
            }, 0);
        this.setState({purchasable: num > 0})

    }

    addIngredientHandler = (key) => {

        const newIngredients = {...this.state.ingredients};
        newIngredients[key] = parseInt(newIngredients[key], 10) + 1;
        const price = this.state.totalprice + INGREDIENT_PRICE[key];
        this.setState({ingredients: newIngredients, totalprice: price});

        this.updatePurchasable(newIngredients);
    }

    removeIngredientHandler = (key) => {

        const newIngredients = {...this.state.ingredients};
        const ingredientCount = newIngredients[key];
        let price = this.state.totalprice;
        if (ingredientCount > 0) {
            newIngredients[key] = ingredientCount - 1;
            price -= INGREDIENT_PRICE[key];
        }

        this.setState({ingredients: newIngredients, totalprice: price});
        this.updatePurchasable(newIngredients);
    }
    showModalHandler = () => {
        const show = this.state.showModal;
        this.setState({showModal: !show})
    }

    cancelPurchase = () => {
        const newState = {
            ingredients: {Salad: 0, Meat: 0, Bacon: 0, Cheese: 0},
            totalprice: 2,
            purchasable: false,
            showModal: false,
        }

        this.setState({...newState});
    }

    performPurchase =  () => {
        console.log(this.props);
        this.props.history.push("/checkout", this.state.ingredients);
        // this.setState({loading: true})
        // try {
        //     const {showModal, purchasable, loading, ...data} = this.state;
        //     const result = await axios.post('/orders.json', data);
        //     console.log('result ', result);
        // } catch (e) {
        //     alert('The purchase could not be proceed');
        // }
        // this.closeModal();
        // this.setState({
        //     loading: false,
        //     totalprice: 2,
        //     purchasable: false,
        //     showModal: false,
        // });
    }

    closeModal = () => {
        this.setState({showModal: false})
    }

    async componentDidMount() {
        try {
            const response = await axios.get('/ingredients.json');
            const ingredients = response.data;
            this.setState({ingredients});
        } catch (e) {
            console.log(e.message)
        }

    }

    render() {
        let burger = <Spinner/>;
        let orderSummary = null;

        if (this.state.ingredients) {
            burger = <Aux>
                    <Burger ingredients={this.state.ingredients}/>
                    <BuildControls
                        ingredients={this.state.ingredients}
                        addIngredient={this.addIngredientHandler}
                        removeIngredient={this.removeIngredientHandler}
                        totalprice={this.state.totalprice}
                        isPurchasable={this.state.purchasable}
                        showmodal={this.showModalHandler}
                    />
            </Aux>

            orderSummary = <OrderSummary
                ingredients={this.state.ingredients}
                totalPrice={this.state.totalprice}
                cancel={this.cancelPurchase}
                purchase={this.performPurchase}
            />;
        }

        if (this.state.loading) {
            orderSummary = <Spinner/>
        }
        return (
            <Aux>
                <Modal showModal={this.state.showModal} closed={this.closeModal}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        )
    }
}

export default BurgerBuilder;
