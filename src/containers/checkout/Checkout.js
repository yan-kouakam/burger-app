import React, { Component } from 'react';
import { Route } from "react-router-dom";

import CheckoutSummary from '../../components/Orders/CheckoutSummary/CheckoutSummary';
import ContactData from './ContactData';

export default class Checkout extends Component {
    state = {}

    cancelPurchase = ()=>{
      this.props.history.goBack();
    }

    continuePurchase = ()=>{
      this.props.history.replace( '/checkout/contact-data', this.state)
    }

    componentWillMount(){
      const ingredient = this.props.location.state;
      this.setState(ingredient)
    }
  render() {
    return (
      <div>
        <CheckoutSummary 
        cancel = {this.cancelPurchase} 
        purchase = {this.continuePurchase}
        ingredients={this.props.location.state}/>

<div>
      <Route path={this.props.match.path + '/contact-data'} 
      render={()=> <ContactData/>}/>
      </div>
      </div>
    )
  }
}
